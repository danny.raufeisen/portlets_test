{if $isPreview}
    {$data=['portlet' => $instance->getDataAttribute()]}
{/if}
{alert data=$data|default:null variant=$instance->getProperty('type-select')
class=$instance->getAnimationClass()|cat:' '|cat:$instance->getStyleClasses()
style=$instance->getStyleString()}
    {$instance->getProperty('some-text')|nl2br}
{/alert}
